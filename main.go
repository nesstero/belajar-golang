package main

import (
	"fmt"
)

func main() {
	// type data
	// string
	nama := "Nestero"
	// int
	usia := 25
	// float
	phi := 3.14
	// boolean
	linuxer := true
	gamer := false

	fmt.Println("nama saya", nama, "usia", usia, "pengguna linux", linuxer, "gamer", gamer, "phi dari lingkaran", phi)
}

func percabangan() {
	// if
	cuaca := "hujan"
	if cuaca == "hujan" {
		fmt.Println("Tidak boleh keluar rumah")
	}

	// if else
	nilai := 70
	var kelakuan string

	if nilai >= 60 {
		fmt.Println("Lulus")
	} else {
		fmt.Println("Tidak Lulus")
	}

	// else if
	if nilai >= 90 {
		fmt.Println("Lulus dengan nilai memuaskan")
	} else if nilai >= 60 {
		fmt.Println("Lulus")
	} else {
		fmt.Println("Tidak Lulus")
	}

	// if bersarang
	kelakuan = "buruk"
	if nilai >= 60 {
		if kelakuan == "baik" {
			fmt.Println("Lulus")
		} else {
			fmt.Println("Tidak Lulus")
		}
	}
}

func perulangan() {
	for i := 1; i <= 7; i++ {
		fmt.Println(i)
	}

	x := 1
	for x <= 5 {
		fmt.Println(x)
		x++
	}
}

func strukturData() {
	// array
	buah := [...]string{"mangga", "apel", "nanas"}
	fmt.Println(buah)

	// merubah nilai
	buah[0] = "jeruk"

	// mengambil nilai dengan perulangan
	// "_" adalah variabel penampung yang tidak digunakan

	for _, macam := range buah {
		fmt.Println(macam)
	}

	// slice
	sayur := []string{"tomat", "kangkung"}

	// menambah nilai
	sayur = append(sayur, "terong")
	fmt.Println(sayur)

	// map
	bio := map[string]string{"nama": "nestero", "hobi": "bermain game"}
	fmt.Println(bio["nama"])

	// mengahpus nilai
	delete(bio, "hobi")
	fmt.Println(bio)

	// mengecek key
	_, keberadaan := bio["usia"]
	fmt.Println(keberadaan)
}
